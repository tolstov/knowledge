### loops

<small>for</small>
```js
const ids = [1,2,3,4,5];
for (var index = 0; index < ids.length; index++) {
	console.log(ids[index]);
}
```

<small>forEach</small>
```js
const ids = [1,2,3,4,5];
//not available break, continue;
ids.forEach(function (id) {
	console.log(id);
});
```

<small>for of</small>
```js
const ids = [1,2,3,4,5];
for (const id of ids) { console.log(id); }
```

<small>for in</small>
```js
//(be careful)
const ids = [1,2,3,4,5];
for (const index in ids) { console.log(ids[index]); }
Array.prototype.myIterator = () => { }
```
