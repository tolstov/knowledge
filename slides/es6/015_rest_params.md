### rest params
<small>The rest parameter syntax allows us to represent an indefinite number of arguments as an array.</small>

```js
//ES5
function foo(name, age, car1, car2) {
	console.log(name, age, car1, car2);
}
foo('Tom', 22, 'Audi', 'BMW');
```

```js
//ES6
function foo(name, age, ...cars) {
	console.log(name, age, ...cars);
}
foo('Tom', 22, 'Audi', 'BMW');
```
