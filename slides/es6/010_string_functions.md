### string functions

<small>startsWith()</small>
```js
const str1 = 'Saturday night plans';

console.log(str1.startsWith('Sat'));//true

```
<small>endsWith()</small>
```js
const str1 = 'Cats are the best!';

console.log(str1.endsWith('best', 17));//true
````

<small>includes()</small>
```js
const sentence = 'The quick brown fox jumped over the lazy dog.';

const word = 'fox';

console.log(`The word "${word}" ${sentence.includes(word)? 'is' : 'is not'} in the sentence`);
//"The word "fox" is in the sentence"
```

<small>repeat()</small>
```js
const chorus = 'Because I\'m happy. ';

console.log('Chorus lyrics for "Happy": ' + chorus.repeat(27));
//"Chorus lyrics for "Happy": Because I'm happy. Because I'm happy. Because I'm happy. Because I'm happy. Because I'm happy. Because I'm happy. Because I'm happy. Because I'm happy. Because I'm happy. Because I'm happy. Because I'm happy. Because I'm happy. Because I'm happy. Because I'm happy. Because I'm happy. Because I'm happy. Because I'm happy. Because I'm happy. Because I'm happy. Because I'm happy. Because I'm happy. Because I'm happy. Because I'm happy. Because I'm happy. Because I'm happy. Because I'm happy. Because I'm happy. "
```