### object literals
<small>ECMAScript 6 makes declaring object literals even more succinct by providing shorthand syntax for initializing properties from variables and defining function methods. It also enables the ability to have computed property keys in an object literal definition.</small>

```js
//ES5
function getCar(make, model, value) {
	return { make: make, model: model, value: value };
}
```

```js
//ES6
function getCar(make, model, value) {
	return { make, model, value };
}
```

```js
//ES5
function getCar(make, model, value) {
	var car = {};
	
	car['make' + make] = true;
	car[model] = model;
	car[value] = value;
	return car;
}
```

```js
//ES6
function getCar(make, model, value) {
	return {
		['make' + make]: true,
		model,
		value
	};
}
```




