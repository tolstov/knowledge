### loops objects

<small>Object.values</small>
```js
const user = { 'id': 24, 'name': 'John Doe'};

for (const value of Object.values(user)) {
	console.log(value);
}
```


<small>Object.keys</small>
```js
const user = { 'id': 24, 'name': 'John Doe'};

for (const key of Object.keys(user)) {
	console.log(key);
}
```

<small>for in</small>

```js
const user = { 'id': 24, 'name': 'John Doe'};

for (const key in user) {
	console.log(key, user[key]);
}
```
