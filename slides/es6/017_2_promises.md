### Promises

<small>Promise.all</small>
```js
const bulbasaur = fetch('https://pokeapi.co/api/v2/pokemon/1/');
const charmander = fetch('https://pokeapi.co/api/v2/pokemon/4/');


Promise.all([bulbasaur, charmander])
	.then((responses) => Promise.all(responses.map(res => res.json())))
	.then((responses) => {
		console.log(responses[0]);
		console.log(responses[1]);
		console.log('Done');
	})
	.catch((error) => console.log(error));
```



