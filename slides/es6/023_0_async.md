### Async/Await

<small>The async function declaration defines an asynchronous function, which returns an AsyncFunction object. An asynchronous function is a function which operates asynchronously via the event loop, using an implicit Promise to return its result. But the syntax and structure of your code using async functions is much more like using standard synchronous functions.</small>
```js
const makeRequest = () => {
  const promise = fetch('https://pokeapi.co/api/v2/pokemon/1/');
  promise.then((data)=> data.json())
    .then((data) => {
      console.log(data);
      console.log('Done');
    });
  console.log('next line');
};
makeRequest();
```
```js
const makeRequest = async () => {
  await fetch('https://pokeapi.co/api/v2/pokemon/1/')
          .then((data)=> data.json())
          .then((data) => {
              console.log(data);
              console.log('Done');
          });
  console.log('next line');
}
makeRequest();
```