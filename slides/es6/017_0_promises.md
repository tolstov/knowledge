### Promises
<small>The Promise object represents the eventual completion (or failure) of an asynchronous operation, and its resulting value.</small>

<small>Fetch</small>
```js
const response = fetch('https://pokeapi.co/api/v2/pokemon/1/');
console.log(response);
console.log('Done');
```

```js
const promise = fetch('https://pokeapi.co/api/v2/pokemon/1/');
promise.then((data)=> data.json())
	.then((data) => {
		console.log(data);
		console.log('Done');
	})
	.catch((error) => console.log(error));
```
