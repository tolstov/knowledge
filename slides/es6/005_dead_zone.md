### dead zone

```js
console.log(foo);
var foo = 'Hi!';
```

```js
console.log(foo);
//<--
//<-- DEAD ZONE
//<--
let foo = 'Hi!';
```