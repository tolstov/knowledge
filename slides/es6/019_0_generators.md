### Generators
<small>The function* declaration (function keyword followed by an asterisk) defines a generator function, which returns a Generator object.</small>
```js
function* Trafficlight() {
  yield 'Red';
  yield 'Yellow';
  yield 'Green';
}

const trafficlight = Trafficlight();
trafficlight.next();
trafficlight.next();
trafficlight.next();
trafficlight.next();
```