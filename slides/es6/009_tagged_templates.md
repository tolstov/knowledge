### tagged templates

```js
const person = 'Fool';
const age = 28;

function sanitaze(strings, personExp, ageExp) {
	const str0 = strings[0]; // "That "
	const str1 = strings[1]; // " is a "

	let personStr;
	if (['Fool', 'Idiot'].includes(personExp)){
		personStr = 'person';
	} else {
		personStr = personExp;
	}

	return `${str0}${personStr}${str1}${ageExp}`;
}

const output = sanitaze`That ${ person } is a ${ age }`;

console.log(output); //That person is a 28
```