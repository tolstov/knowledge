### functions default values

```js
function taxes(total, tax = 0.15) {
	return total * tax;
}
taxes(100);

taxes(100, undefined); //Correct
taxes(100, 0); //Not correct
taxes(100, null);//Not correct
```