### interpolations
<small>Template literals are string literals allowing embedded expressions. You can use multi-line strings and string interpolation features with them. They were called "template strings" in prior editions of the ES2015 specification.</small>
```js
//ES5
var age = 28;
var name = 'Bob';
console.log('Person: ' + name + ', age: ' +age);
```


```js
//ES6
const age = 28;
const name = 'Bob';
console.log(`Person: ${name}, age: ${age}`);
```