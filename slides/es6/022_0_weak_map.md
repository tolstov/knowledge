### WeakMap
<small>The WeakMap object is a collection of key/value pairs in which the keys are weakly referenced.  The keys must be objects and the values can be arbitrary values.</small>
```js
let tom = {name: 'Tom' };
const student = new WeakMap();
student.set(tom, 'Description');
console.log(student);
tom = null;
```