### spread operator

<small>Spread syntax allows an iterable such as an array expression or string to be expanded in places where zero or more arguments (for function calls) or elements (for array literals) are expected, or an object expression to be expanded in places where zero or more key-value pairs (for object literals) are expected.</small>

```js
//ES5
var vegetables = ['tomato', 'beet', 'potato'];
var fruits = ['apple', 'pineapple'];
var water = 'water';

var cart = vegetables.concat(fruits);
cart.push(water);
```

```js
//ES6
const vegetables = ['tomato', 'beet', 'potato'];
const fruits = ['apple', 'pineapple'];
const water = 'water';

let cart = [...vegetables, ...fruits, water];
```

```js
//Immutable
const vegetables = ['tomato', 'beet', 'potato'];
const fruits = ['apple', 'pineapple'];
const water = 'water';

let cart = [...vegetables, ...fruits, water];
let myFriendCart = cart; //let myFriendCart = [...cart];
myFriendCart[0] = 'garlic';
console.log(myFriendCart);
console.log(cart);
```