### var problems

```js
//Problem: overrides system variables
var screen = '15.6';
console.log(screen);
console.log(window.screen);

//Solutions ES5
(function () {
	var screen = '15.6';
	console.log(screen);
	console.log(window.screen);
})();
//Solutions ES6
{
  let screen = 15.6;
  console.log(screen);
  console.log(window.screen);
}
```
```js
//Problem: loop with side effect
for(var i = 0; i < 10; i++) {
  setTimeout(function() {
    console.log(i);
  }, 1000);
}
```
