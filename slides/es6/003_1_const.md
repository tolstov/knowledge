### const

<small>Constants are block-scoped, much like variables defined using the let statement. The value of a constant cannot change through reassignment, and it can't be redeclared.</small>
```js
const age = 28;

age+=3;

const person = {
	age: 28,
	name: 'Tom'
};
person.age = 30;


Object.freeze(person);

person.age = 30;
```
