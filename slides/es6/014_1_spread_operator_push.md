### spread operator

Problem
```js
const vegetables = ['tomato', 'beet', 'potato'];
const other_vegetables = ['garlic', 'pepper'];

vegetables.push(other_vegetables);
console.log(vegetables);
```

Solution
```js
const vegetables = ['tomato', 'beet', 'potato'];
const other_vegetables = ['garlic', 'pepper'];

vegetables.push(...other_vegetables);
console.log(vegetables);
```
