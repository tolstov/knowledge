### OOP

<small>Extending Arrays</small>
```js
class Orders extends Array{
  constructor(orders) {
    super(...orders);
  }
  
  add(id, name) {
    this.push( { id, name });
  }
}

const orders = new Orders([
  { id: 1, name: 'Apple' },
  { id: 2, name: 'Banana' },
  { id: 4, name: 'Milk' },
]);
console.log(orders);
```