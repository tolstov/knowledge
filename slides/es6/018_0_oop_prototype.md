### OOP Prototype
<small>Prototypes are the mechanism by which JavaScript objects inherit features from one another. In this article, we explain how prototype chains work and look at how the prototype property can be used to add methods to existing constructors.</small>
```js
function Person(name, age) {
  this.name = name;
  this.age = age;
}

const tom = new Person('Tom', 22);

console.log(tom);

```

```js
function Person(name, age) {
  this.name = name;
  this.age = age;
}

const tom = new Person('Tom', 22);

Person.prototype.say = function(message) {
  console.log(message);
}
tom.say('Hi!')
console.log(tom);
```
