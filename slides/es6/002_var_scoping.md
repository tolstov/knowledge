### var Scouping
			
```js
//Root scope
var age;
window.age;
age=28;


function changeAge() {
  age = 30;
}

changeAge();

//Scope in functions

function addAge(value) {
  var addedAge = value;
  age += value;
}

addAge(30);

//Scope in blocks

if(age > 60) {
  var retired = true;
}
console.log(retired);
```