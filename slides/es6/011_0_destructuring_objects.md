### destructuring objects
<small>The destructuring assignment syntax is a JavaScript expression that makes it possible to unpack values from arrays, or properties from objects, into distinct variables.</small>
```js
const currentUser = {
	'id': 24,
	'name': 'John Doe',
	'website': 'http://mywebsite.com',
	'description': 'I am an actor',
	'email': 'example@example.com',
	'gender': 'M',
	'phone_number': '+12345678',
	'username': 'johndoe',
	'birth_date': '1991-02-23',
	'followers': 46263,
	'following': 345,
	'like': 204,
	'comments': 9
};

const { id, username } = currentUser;
```

Custom name
```js
const { id: userId, username } = currentUser;
```

With default value
```js
const { id, username, name= 'Anonymous' } = currentUser;
```