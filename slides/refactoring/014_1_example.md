### Example

```ruby
class OffersController < ApplicationController
  before_filter :authenticate_user!

  def create
    @offer = Offer.new(offer_params)

    if @offer.company_id == current_user.company_id || !@offer.order.active?
      flash[:error] = 'You are not authorized to perform this action.'
      redirect_to root_path
    end

    @offer.save
    redirect_to @offer
  end

  private

  def offer_params
    params.require(:offer).permit(:order_id, :company_id, :amount)
  end
end
```