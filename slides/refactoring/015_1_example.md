### Example

```ruby
Name:
= @user.first_name + ' ' @user.last_name

Email:
- if @user.email_private?
  'Private'
- else
  = @user.email

Birthday:
= @user.birthday.strftime('%d. %m. %Y.')
```