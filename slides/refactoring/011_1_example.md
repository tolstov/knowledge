### Bad solution

```ruby
# app/controllers/registration_controller.rb
class RegistrationsController < ApplicationController
  def new
    @user = User.new
  end

  def create
    @user = User.new(resource_params)
    if @user.valid?
      @user.first_name = @user.full_name.split(' ').first
      @user.last_name = @user.full_name.split(' ')[1..-1].to_a.join(' ')
      @user.save
      @company = Company.create(name: @user.company_name, phone: @user.phone)
      @company.users << @user
      redirect_to root_path
    else
      render :new
    end
  end
  
  private
  
  def resource_params
    params.require(:user).permit(:full_name, :company_name, :phone, :email)
  end
end
```