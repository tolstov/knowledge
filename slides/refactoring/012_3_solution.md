### Solution

```ruby
# app/queries/articles_query.rb
class ArticlesQuery
  attr_accessor :initial_scope

  def initialize(initial_scope = Article.all)
    @initial_scope = initial_scope
  end
  
  def call(:min_view_count, :author_name)
    initial_scope.published.minimal_view_count(view_count).author_first_name_like(first_name)
  end
  
  private

  def published
    relation.where(published: true)
  end

  def minimal_view_count(view_count)
    relation.where('view_count > ?', view_count)
  end

  def author_first_name_like(first_name)
    with_authors.where('users.first_name LIKE ?', "#{first_name}%")
  end

  def with_authors
    relation.joins('LEFT OUTER JOIN users ON users.id = articles.author_id')
  end
end
```

> ! Query object should always return Active Record Relation
