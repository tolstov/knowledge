### Form object​

The role of the Form Object is to manage the input data for a given action. It validates data and only allows whitelisted attributes (replacing the need for Strong Parameters).

This is a departure from "The Rails Way" where the model contains the validations. Form Objects help to reduce the weight of your models for one, but also, in an app of reasonable complexity even simple things like validations become harder because context is important.