### Example

```ruby
# app/controllers/articles_controller.rb
class ArticlesController < ApplicationController
  def index
    @articles = Article
                  .joins('LEFT OUTER JOIN users ON users.id = articles.author_id')
                  .where(published: true)
                  .where('view_count > ?', params[:min_view_count])
                  .where('users.first_name LIKE ?', "#{params[:author_name]}%")
end
```