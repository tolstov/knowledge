### Example

```ruby
# app/models/image.rb
class Image < ActiveRecord::Base
  validates :title, presence: true
  validate :valid_file_name_extension

  private

  def valid_file_name_extension
    if file_name.present? && !%w{.jpg .png .jpeg}.include?(File.extname(file_name.downcase))
      errors.add(:file_name, :invalid_extension)
    end
  end
end
```