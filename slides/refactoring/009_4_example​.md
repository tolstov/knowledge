### Solution
```ruby
# app/services/tweet_creator.rb
class TweetCreator
  attr_reader :message
  
  def initialize(message)
    @message = message
  end

  def call
    client = Twitter::REST::Client.new do |config|
      config.consumer_key        = ENV['TWITTER_CONSUMER_KEY']
      config.consumer_secret     = ENV['TWITTER_CONSUMER_SECRET']
      config.access_token        = ENV['TWITTER_ACCESS_TOKEN']
      config.access_token_secret = ENV['TWITTER_ACCESS_SECRET']
    end
    client.update(@message)
  end
end
```
```ruby
# app/controllers/tweet_controller.rb
class TweetController
  def create
    TweetCreator.new(params[:message]).call
  end
end
```
> ! Service should contains only one public method call/execute/run