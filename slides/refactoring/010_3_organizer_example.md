### Problem

```ruby
# app/interactors/facebook_creator.rb
class FacebookCreator
  include Interactor

  def call(message)
      client = Facebook::Post(messge: message)
      if client.run
        context.response = client.response
      else
        context.fail!(message: client.error)
      end
  end
end
```

```ruby
result = TweetCreator.call(params[:message])
if result.success?
    result = FacebookCreator.call(params[:message])
    if result.success?
      return redirect_to profile_path
    end
end
flash.now[:message] = result.message
render :profile
```

