### Service Object​

Service Objects (also known as Commands) are the home of your business logic. They allow you to simplify your models and controllers and allow them to focus on what they are responsible for. A Service Object should encapsulate a single user task such as registering for a new account or placing an order.