### Example

```ruby
class User < ActiveRecord::Base
  after_create :send_account_welcome_email
  after_destroy :send_account_deleted_email
  
  private
  
  def send_account_welcome_email
    UserMailer.welcome_user(email).deliver_later
  end
  
  def send_account_deleted_email
    UserMailer.delete_user(email).deliver_later
  end
end
```