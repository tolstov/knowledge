### Solution

```ruby
# app/interactors/social_network_sender.rb
class SocialNetworkSender
  include Interactor::Organzier
  
  organize TweetCreator, FacebookCreator
end
```



```ruby
# in your controller
result = SocialNetworkSender.call(params[:message])
```

