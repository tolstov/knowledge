### Interactor interface

```ruby
# app/interactors/create_order.rb
class CreateOrder
  include Interactor

  def call(id)
    context.my_var = "My var"
    context.fail!(error: "Boom!")
  end

  def rollback
    # callback after fail
  end
end

create_order = CreateOrder.call(101)
create_order.success? # => false
create_order.failure? # => true
create_order.my_var # => "My var"
```