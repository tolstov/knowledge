### Policy object​

When it comes to authorization (verifying if current_user has permission to do stuff he/she is requesting to) it’s a different topic. 
Yes there are several solutions out there that works well on small project (CanCanCan, Rolify, …) but once your project grows to medium to large scale then these generic solutions may become a burden.