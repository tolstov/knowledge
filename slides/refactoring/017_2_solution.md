### Solution

```ruby
# app/validators/image_file_name_validator.rb
class ImageFileNameValidator < ActiveModel::EachValidator
  def validate_each(record, attribute, value)
  end
end
```

```
# app/models/image.rb
class Image < ActiveRecord::Base
  validates :title, presence: true
  validates :file_name, image_file_name: { supported_extensions: %w{.jpg .png .jpeg} }
end
```