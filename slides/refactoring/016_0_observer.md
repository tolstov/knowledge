### Observer

Observer respond to life cycle callbacks to implement trigger-like behavior outside the original class.