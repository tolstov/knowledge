### When to refactor

* Rule of Three
* When adding a feature
* When fixing a bug
* During a code review