<img class="img-circle" src="https://pbs.twimg.com/profile_images/625867214084661248/OWnMCwiY_400x400.jpg">
<br>
<small>Bryan Helmkamp</small>
<br>
<small>Founder and CEO, codeclimate</small>

Some actions in a system warrant a Service Object to encapsulate their operation. I reach for Service Objects when an action meets one or more of these criteria:


* The action is complex (e.g. closing the books at the end of an accounting period)
* The action reaches across multiple models (e.g. an e-commerce purchase using Order, CreditCard and Customer objects)
* The action interacts with an external service (e.g. posting to social networks)
* The action is not a core concern of the underlying model (e.g. sweeping up outdated data after a certain time period).