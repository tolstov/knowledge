### Example Interactor

```ruby
# app/interactors/tweet_creator.rb
class TweetCreator
  include Interactor

  def call(message)
      client = Twitter::REST::Client.new do |config|
        config.consumer_key        = ENV['TWITTER_CONSUMER_KEY']
        config.consumer_secret     = ENV['TWITTER_CONSUMER_SECRET']
        config.access_token        = ENV['TWITTER_ACCESS_TOKEN']
        config.access_token_secret = ENV['TWITTER_ACCESS_SECRET']
      end
      if client.update(@message)
        context.response =  client.response
      else
        context.fail!(message: client.error)
      end
  end
end
```

```diff
# app/controllers/tweet_controller.rb
class TweetController < ApplicationController
  def create
     result = TweetCreator.call(params[:message])
+    if result.success?
+      redirect_to profile_path
+    else
+      flash.now[:message] = result.message
+      render :profile
+    end
  end
end
```

