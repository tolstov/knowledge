```ruby
# app/controllers/articles_controller.rb
class ArticlesController < ApplicationController
  def index
    @articles = ArticlesQuery.new.call(params)
  end
end
```

