### Solution

```ruby
# app/decorators/user_decorator.rb
class UserDecorator < SimpleDelegator
  def full_name
    first_name + " " + last_name
  end

  def protected_email
    return "Private" if email_private?
    email
  end

  def formatted_birthday
    birthday.strftime("%d %b %Y")
  end
end
```

```ruby
# app/controllers/users_controller.rb
class UsersController < ApplicationController
  def show
    user = User.find(params[:id])
    @user = UserDecorator.new(user)
  end
end
```

