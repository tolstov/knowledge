### Example

```ruby
# app/entities/money_entity.rb
class MoneyEntity
  attr_reader :currency, :amount

  def initialize(amount, currency)
    @amount = amount
    @currency = currency
  end
end
```

```ruby
# REPL
mikes_salary = MoneyEntity.new('USD', 1000)
toms_salary = MoneyEntity.new('USD', 1000)
mikes_salary == toms_salary
=> false
```