<img class="img-circle" src="https://pbs.twimg.com/profile_images/768528083070705666/mIFI3WYo.jpg">
<br>
<small>Eric Evans</small>
<br>
<small>Founder of Domain Language</small>

<b>Service</b>: A standalone operation within the context of your domain.

A <b>Service Object</b> collects one or more services into an object. Typically you will have only one instance of each service object type within your execution context.