### Value object​

A small simple object, like money or a date range, whose equality isn’t based on identity.