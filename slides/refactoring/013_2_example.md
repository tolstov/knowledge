### Solution

```ruby
# app/entities/money_entity.rb
class MoneyEntity
  attr_reader :currency, :amount

  def initialize(amount, currency)
    @amount = amount
    @currency = currency
  end
  
  def ==(other_money)
      self.class == other_money.class &&
      amount == other_money.amount &&
      currency == other_money.currency
  end
  
  def hash
    [@amount, @currency].hash
  end
end
```

```ruby
# REPL
mikes_salary = MoneyEntity.new('USD', 1000)
toms_salary = MoneyEntity.new('USD', 1000)
mikes_salary == toms_salary
=> true
```