### Decorators

The decorator pattern is used to wrap an object and extend its functionality without modifying the object itself.
It's similar to the presenter and adapter patterns, which also wrap an object (or multiple objects), but it provides a different functionality compared to those two patterns