### Rails-observers/Wisper


```bash
# Gemfile
gem "wisper"
gem "wisper-activerecord"

```


```ruby
# app/observers/user_observer.rb
class UserObserver
  def after_create(user)
    UserMailer.welcome_user(user.email).deliver_later
  end

  def after_destroy(user)
    UserMailer.delete_user(user.email).deliver_later
  end
end
```


```ruby
# config/initializers/wisper.rb
Rails.application.config.to_prepare do
  User.subscribe(UserObserver.new)
end
```