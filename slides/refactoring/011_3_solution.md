```ruby
# app/controllers/registration_controller.rb
class RegistrationsController < ApplicationController
  def new
    @form = RegistrationForm.new
  end

  def create
    @form = RegistrationForm.new(resource_params)
    if @form.save
      redirect_to root_path
    else
      render :new
    end
  end

  private

  def resource_params
    params.require(:registration).permit(:full_name, :company_name, :phone, :email)
  end
end
```